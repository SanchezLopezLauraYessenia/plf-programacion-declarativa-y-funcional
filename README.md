# 1.- Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap
*[#F5C0C0] Programación 
	* Tiene dos partes
		*[#9FD8DF] Creativa
			*[#F0E4D7] Creación del algoritmo
		*[#9FD8DF] Burocrática
			*[#F0E4D7]  Gestión detallada 
				*_ De 
					*[#FFFFFF] La memoria
			*[#F0E4D7]  Secuencia de las ordenes
	* Concepto
		*[#F0E4D7]  Decirle a la máquina lo\n que se quiere que haga
			
	* Evolución
		*_ Al inicio
			*[#9FD8DF] Los Programas 
				*_ Consistían en 
					*[#F0E4D7]  Secuencias de órdenes directas
				*_ Lenguajes
					*[#F0E4D7]  Bajo nivel
					*[#F0E4D7]  Inetrmedios
					*[#F0E4D7]  Alto nivel
	* Tipos 
		*_ Surgen por
			*[#F0E4D7]  Problemas que se lleva con \nla programación clásica

		*[#FF7171] Declarativa
			*_ Consiste
				*[#F0E4D7]  Tareas rutinarias de programación se dejan al compilador
					*_ Beneficios
						*[#FFFFFF] Más precisión en la practica
						*[#FFFFFF]  Mejor tiempo de verificacion y modificación
				*_ Abandonar 
					*[#F0E4D7] Secuencias de órdenes que \ngestiona la memoria del ordenador 
			*_ Dos varinates
				* Funcional
					*[#F0E4D7]  Tomar el modelo en el que los \nmatemáticos definen funciones
						*_ Mediante
							*[#FFFFFF] Reglas de reescritura 
							*[#FFFFFF] Simplificación de expresiones
					*_ Caracteristicas
						*[#F0E4D7]  Definir tipos de datos infinitos
						*[#F0E4D7]  Evaluación perezosa
							*[#FFFFFF] Aquello que es estrictamente necesario
						*[#F0E4D7]  Funciones de orden superior
							*_ Cumplen con
								*[#FFFFFF]  Tomar una o más funciones como entrada
								*[#FFFFFF] Devolver una función como salida	
					*_ Lenguajes de\n  progrmación
						*[#F0E4D7] LISP
							*_ Es
								*[#FFFFFF] Un lenguaje híbrido 
							*_ Carece de
								*[#FFFFFF] Sistemas de inferencia de tipos 	
						*[#F0E4D7] Haskell
							*_ Es
								*[#FFFFFF] Lenguaje estándar
				* Lógica
					*[#F0E4D7]  Se acude a la lógica de predicados\n             de primer orden
						*_  Define
							*[#FFFFFF] Funciones que actúan sobre 
								*[#FFFFFF] Funciones 
								*[#FFFFFF] Datos primitivos.
					*_ Ventajas
						*[#FFFFFF] Permite 
							*[#FFFFFF] Ser más declarativos
							*[#FFFFFF] Procesos inversos entre argumentos y resultados
						*[#FFFFFF] Establece dirección\n de procesamiento
							*_ Mediante
								*[#FFFFFF] Argumentos
								*[#FFFFFF] Resultados
					*_ Utiliza
						*[#FFFFFF] Axiomas
						*[#FFFFFF] Reglas de Inferencia
					*_ Lenguajes 
						*[#FFFFFF] Prolog 
					*_ Ejemplo
						*[#FFFFFF] Definir una relación factorial\n    entre dos números
					*_ Caracteristcas
						*[#FFFFFF] Recursión como estructura \nde control Básica
						*[#FFFFFF] El compilador es un motor de inferencia
					
			*_ Ventajas
				*[#F0E4D7]  Ahorro de tiempo en 
					*[#FFFFFF] Desarrollo
					*[#FFFFFF] Modificación
					*[#FFFFFF] Depuración 
			*_ Desventajas
				*[#F0E4D7]  El compilador toma las decisiones\n            automaticamente
					*_ Genera
						*[#FFFFFF] Es dificil que sean eficientes

			*_ Objetivos
				*[#F0E4D7]  Reducir la complejidad de los programas
				*[#F0E4D7]  Liberarse
					*_ De
						*[#FFFFFF] Las asignaciones
						*[#FFFFFF] Control de memoria del ordenador
				*[#F0E4D7]  Reducir el riesgo de cometer errores haciendo programas
				*[#F0E4D7]  Programas más cortos
			*[#F0E4D7]  Más Complejo en estos \ntérminos de abstracción 

		*[#9FD8DF] Imperativa
			*_ Es
				*[#F0E4D7]  Darle instrucciones
				*[#F0E4D7]  Comandos uno detrás de otro al ordenador
			*_ Se divide 
				*[#F0E4D7]  Estructurada
				*[#F0E4D7]  Procedimental
				*[#F0E4D7]  Modular	
		*[#9FD8DF] Operativa 
			*[#F0E4D7]  Se basa en el modelo de Von Neumann
@endtmindmap
```

# 2.- Lenguaje de Programación Funcional (2015)
```plantuml
@startmindmap
*[#8BCDCD] Programacion funcional
	*[#F1C5C5] Memorización
		*_ Es
			*[#FFFFFF] Almacenar el valor de una expresión \ncuya evaluación ya se realizó
		*_ Aportación de
			*[#FFFFFF]  Donald Michie
		*_ En 
			*[#FFFFFF] 1968
	*[#F1C5C5] Currificación
		*_ Es
			*[#FAF0AF] Una Técnica
				*_ Consiste 
					*[#FFFFFF] Varios parámetros de entrada 
					*[#FFFFFF] Devuelve diferentes salidas
		*_ Sirve para
			*[#FAF0AF] Utilizar funciones como argumentos de otras funciones
		*_ En honor a
			*[#FAF0AF] Haskell Brooks Curry
		*_ Por
			*[#FAF0AF] Moses Schönfinkel y Gottlob Frege.
		
	
	*[#F1C5C5] Evaluacion
		*_ Se divide 
			*[#FAF0AF] Estricta
				*_ Consiste
					*[#FFFFFF] Primero evaluar lo más interno
					*[#FFFFFF] Después lo más externo
			*[#FAF0AF] No estricta
				*_ Es
					*[#FFFFFF] Evaluar desde afuera hacia adentro
					*[#FFFFFF] Es mucho más complicada de implementar
			*[#FAF0AF] En corto circuito
				*_ Utiliza
					*[#FFFFFF]  Operadores booleanos		
	*[#F1C5C5] Paradigma de programación 
		*_ Es
			*[#FFFFFF] Forma en que se abordan los problemas 
		*_ Consiste en 
			*[#FFFFFF] Modelo de computación en el que los diferentes \nlenguajes dotan de semántica a los programas. 
		*_ Los lenguajes estaban\n        basados
			*[#FAF0AF] Modelo de computación\n de Von Neumann
				*_ Propone
					*[#F8EDED] Los programas debían almacenarse en \nla misma máquina antes de ser ejecutados
				*_ Caracteristicas
					*[#F8EDED] Estado del cómputo
						*[#FFFFFF] Es una zona de memoria
							*_ Se accede 
								*[#FFFFFF] Mediante una serie de variables
						*[#FFFFFF] Las variables se modifican a\n través de la asignación	
					*[#F8EDED] La mayoría de los lenguajes siguen este modelo
				*_ Por 
					*[#F8EDED] Matematico John Von Neumann

	*[#F1C5C5] Cálculo Lambda 
		*_ Es
			*[#FFFFFF] Sistema computacional bastante potente 
			*[#FFFFFF] Equivalente
				*[#FFFFFF] Modelo propuesto por John Newman.
		*_ Por 
			*[#FFFFFF] Alonzo Church 
			*[#FFFFFF] Stephen Kleene
		*_ Cuando 
			*[#FFFFFF] En la década de 1930 
		*_ Posteriormente
			*[#F8EDED] Lo continuo Haskell Brooks Curry 
				*[#FFFFFF] Sentó lo que son las bases \nde la programación funcional.
		*_ Nace
			*[#F8EDED] ALGOL
				*_ Por
					*[#FFFFFF]  Peter J. Landin
				*_ En
					*[#FFFFFF] 1965
	*[#F1C5C5] Funciones
		*_ Tipos
			*[#FAF0AF] Funciones de orden superior
				*_ Cumplen con
					*[#FFFFFF] Tomar una o más funciones como entrada
					*[#FFFFFF] Devolver una función como salida
			*[#FAF0AF] Función constante     
				*_ Consiste
					*[#FFFFFF] Devuelve siempre el mismo resultado
		*_ Son
			*[#FAF0AF] En su concepción \npuramente matemática
		*[#FAF0AF] Pueden ser parámetros\n de otras funciones	

	*[#F1C5C5]  Caracteristicas 
		*[#FAF0AF] Estado del computo
		*[#FAF0AF]  Aplicación parcial	
			*[#F8EDED] Aplicación de una función \npero con menos parámetros 
		*[#FAF0AF] Concepto que desaparecen
			*[#F8EDED] Variable
				*_ Existe otro concepto\n para las variables
					*[#FFFFFF] Sirven para referirse a los parámetros\n de entrada de las funciones
			*[#F8EDED] Bucle
				*_ La forma es 
					*[#F8EDED] Mediante la recursión.
					*[#F8EDED] Funciones recursivas
						*_ Son
							*[#FFFFFF] Aquellas en cuya definición se \nhacen referencias a sí mismas
								*_ Ejemplo
									*[#FFFFFF] Factorial de un número
		*[#FAF0AF] El resultado de las funciones \nno dependen de las otras
		*[#FAF0AF] Transparencia referencial
			*_ Consiste
				*[#FFFFFF] Una expresión E del lenguaje puede \nser sustituida por otra de igual valor V
	*[#F1C5C5]  Ventajas
		*[#FAF0AF] Permite escribir codigo de manera rápida
		*[#FAF0AF] No hay dependencia entre funciones
		*[#FAF0AF] Brinda un entorno multiprocesador
			*_ es decir
				*[#FFFFFF] Tener funciones en paralelo
	*[#F1C5C5] Desventajas 
		*[#FAF0AF] Nivel de abstracción demasiado alto		
			
@endtmindmap
```
